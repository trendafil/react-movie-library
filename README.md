#React Movie Library
A small single page application, which allows users to search to Movies DB database.

## Built With 
* [React](https://reactjs.org/) - A JavaScript library for building user interfaces
* [Redux](https://redux.js.org/) - A JavaScript library for managing application state
* [Webpack](https://webpack.js.org/) - A static module bundler
* [npm](https://www.npmjs.com/) - Dependency Management
